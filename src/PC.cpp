#include <systemc.h>
#include "PC.hpp"

using namespace std;
using namespace sc_core;

void reg::behav()
{
   while(true)
   {
      wait();
      if (!rst_n->read())
        out->write(0);
      else
        out->write(in->read());
   }
}
