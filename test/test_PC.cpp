#include <systemc.h>
#include <iostream>
#include "PC.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(5, SC_NS);
const sc_time clk_semiperiod(1, SC_NS);
const unsigned bit_data = 32;

SC_MODULE(TestBench) 
{
 public:
  sc_signal<bool> clk;
  sc_signal<bool> rst_n;
  sc_signal<sc_bv<bit_data> > in;
  sc_signal<sc_bv<bit_data> > out;

  reg PC;

  SC_CTOR(TestBench) : PC("PC")
  {
    SC_THREAD(clk_thread);
    SC_THREAD(rst_thread);
    SC_THREAD(stimulus_thread);
    SC_THREAD(checker_and_watcher);
      sensitive << clk;

    PC.clk(this->clk);
    PC.rst_n(this->rst_n);
    PC.in(this->in);
	  PC.out(this->out);
  }

  bool Check() const {return error;}
  
 private:
  bool error;
  bool done;

  void rst_thread()
  {
    rst_n.write(0);
    wait(2*clk_semiperiod);
    rst_n.write(1);
  }

  void clk_thread()
  {
    done = 0;
    clk.write(0);
    while(!done)
    {
      wait(clk_semiperiod);
      if(clk.read() == 0) clk.write(1);
      else clk.write(0);
    }
  }

  void stimulus_thread() 
  {
    in.write(16);
    wait(wait_time);

    in.write(5);
    wait(wait_time);

    in.write(0);
    wait(wait_time);

    in.write(24);
    wait(wait_time);

    done = 1;
  }

  void checker_and_watcher() 
  {
    error = 0;
    while(true)
    {
      wait();
      wait(1, SC_PS); //to avoid the execution of "if" condition before execution of the component
      //watcher part
      if(clk.read())
      {
        cout << "At time " << sc_time_stamp() << ": "; 
        if(!rst_n.read())  cout << "PC reset; PC output = " << out.read().to_uint() << endl;
        else cout << "output of PC = " << out.read().to_uint() << endl;
      }

      //checker part
      if((in.read() != out.read()) && rst_n.read() && clk.read())
      {
        cout << "test failed!!\bTheoretical value: " << in.read().to_uint() 
             << ";\ntest result: " << out.read().to_uint() << endl << endl;
        error = 1;
      }
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.Check();
}

