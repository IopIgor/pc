#ifndef PC_HPP
#define PC_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(reg) 
{
  static const unsigned bit_data = 32;

  sc_in<bool> clk;
  sc_in<bool> rst_n;
  sc_in<sc_bv<bit_data> > in;
  sc_out<sc_bv<bit_data> > out;
  
  SC_CTOR(reg) 
  {
    SC_THREAD(behav);
      sensitive << clk.pos();
  } 
  
 private:
  void behav();
};

#endif
